# Estate Agent Commission Bid System

## Team Members
- Raees Aziz Muhammad (raees@bbd.co.za)
- Kyle Schoonbee (kyles@bbd.co.za)
- Sashen Naidoo (sashenn@bbd.co.za)
- Ayesha Mohamed (ayesham@bbd.co.za)
- Brad Betts (bradleyb@bbd.co.za)
- Lebo Nzimande (lebohangn@bbd.co.za)

## Assigned Tasks
### Raees Aziz Muhammad
- Views

### Kyle Schoonbee
- User Defined Function

### Sashen Naidoo
- Setup AWS
- Stored Procedures

### Lebo Nzimande
- Stored Procedures

### Ayesha Mohomed 
- ERD 
- Initial Tables and Mock Values

### Brad Betts
- ERD
- User Defined Function with Kyle

**The whole team contributed to the discussion around the ERD and what tables should be used and how to structure the Database**







