USE [master]
GO
/****** Object:  Database [PamellaGoldingDB]    Script Date: 2022/02/21 14:37:02 ******/
CREATE DATABASE [PamellaGoldingDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PamellaGoldingDB', FILENAME = N'D:\rdsdbdata\DATA\PamellaGoldingDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'PamellaGoldingDB_log', FILENAME = N'D:\rdsdbdata\DATA\PamellaGoldingDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PamellaGoldingDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PamellaGoldingDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PamellaGoldingDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PamellaGoldingDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PamellaGoldingDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PamellaGoldingDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PamellaGoldingDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET RECOVERY FULL 
GO
ALTER DATABASE [PamellaGoldingDB] SET  MULTI_USER 
GO
ALTER DATABASE [PamellaGoldingDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PamellaGoldingDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PamellaGoldingDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PamellaGoldingDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PamellaGoldingDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PamellaGoldingDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PamellaGoldingDB] SET QUERY_STORE = OFF
GO
USE [PamellaGoldingDB]
GO
/****** Object:  User [group7DBMain]    Script Date: 2022/02/21 14:37:03 ******/
CREATE USER [group7DBMain] FOR LOGIN [group7DBMain] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [group7DBMain]
GO
/****** Object:  Table [dbo].[Property]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Property_ID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyType_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[ERF] [varchar](50) NULL,
	[NumberOfRooms] [varchar](50) NULL,
	[NumberOfBathrooms] [varchar](50) NULL,
	[StreetNumber] [varchar](50) NULL,
	[StreetName] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
 CONSTRAINT [PK_Property_1] PRIMARY KEY CLUSTERED 
(
	[Property_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Single_family_homes]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Single_family_homes] AS (
	SELECT * FROM PamellaGoldingDB.dbo.Property
	WHERE 
	PropertyType_ID = 1
);
GO
/****** Object:  Table [dbo].[Amenity]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Amenity](
	[Amenity_ID] [int] IDENTITY(1,1) NOT NULL,
	[Amenity_Description] [varchar](50) NULL,
 CONSTRAINT [PK_Amenity] PRIMARY KEY CLUSTERED 
(
	[Amenity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auction]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auction](
	[Auction_ID] [int] IDENTITY(1,1) NOT NULL,
	[Property_ID] [int] NOT NULL,
	[Start_Date] [date] NULL,
	[End_Date] [date] NULL,
	[Location] [varchar](100) NULL,
 CONSTRAINT [PK_Auction] PRIMARY KEY CLUSTERED 
(
	[Auction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bid]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bid](
	[Bid_ID] [int] IDENTITY(1,1) NOT NULL,
	[Auction_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Amount] [money] NULL,
	[Details] [varchar](max) NULL,
 CONSTRAINT [PK_Bid] PRIMARY KEY CLUSTERED 
(
	[Bid_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropertyAmenity]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyAmenity](
	[Amenity_ID] [int] NOT NULL,
	[Property_ID] [int] NOT NULL,
 CONSTRAINT [PK_PropertyAmenity] PRIMARY KEY CLUSTERED 
(
	[Amenity_ID] ASC,
	[Property_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropertyType]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyType](
	[PropertyType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_PropertyType] PRIMARY KEY CLUSTERED 
(
	[PropertyType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[User_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserType_ID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Cell_Number] [varchar](15) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 2022/02/21 14:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[UserType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Auction]  WITH CHECK ADD  CONSTRAINT [FK_Auction_Property] FOREIGN KEY([Property_ID])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[Auction] CHECK CONSTRAINT [FK_Auction_Property]
GO
ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Auction] FOREIGN KEY([Auction_ID])
REFERENCES [dbo].[Auction] ([Auction_ID])
GO
ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Auction]
GO
ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_User]
GO
ALTER TABLE [dbo].[Property]  WITH CHECK ADD  CONSTRAINT [FK_Property_PropertyType] FOREIGN KEY([PropertyType_ID])
REFERENCES [dbo].[PropertyType] ([PropertyType_ID])
GO
ALTER TABLE [dbo].[Property] CHECK CONSTRAINT [FK_Property_PropertyType]
GO
ALTER TABLE [dbo].[Property]  WITH CHECK ADD  CONSTRAINT [FK_Property_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([User_ID])
GO
ALTER TABLE [dbo].[Property] CHECK CONSTRAINT [FK_Property_User]
GO
ALTER TABLE [dbo].[PropertyAmenity]  WITH CHECK ADD  CONSTRAINT [FK_PropertyAmenity_Amenity] FOREIGN KEY([Amenity_ID])
REFERENCES [dbo].[Amenity] ([Amenity_ID])
GO
ALTER TABLE [dbo].[PropertyAmenity] CHECK CONSTRAINT [FK_PropertyAmenity_Amenity]
GO
ALTER TABLE [dbo].[PropertyAmenity]  WITH CHECK ADD  CONSTRAINT [FK_PropertyAmenity_Property] FOREIGN KEY([Property_ID])
REFERENCES [dbo].[Property] ([Property_ID])
GO
ALTER TABLE [dbo].[PropertyAmenity] CHECK CONSTRAINT [FK_PropertyAmenity_Property]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserType] FOREIGN KEY([UserType_ID])
REFERENCES [dbo].[UserType] ([UserType_ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserType]
GO
USE [master]
GO
ALTER DATABASE [PamellaGoldingDB] SET  READ_WRITE 
GO
