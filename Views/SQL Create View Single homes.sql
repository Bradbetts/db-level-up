USE PamellaGoldingDB
GO

CREATE VIEW Single_family_homes AS (
	SELECT * FROM PamellaGoldingDB.dbo.Property
	WHERE 
	PropertyType_ID = 1
);