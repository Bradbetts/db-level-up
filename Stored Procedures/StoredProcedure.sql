/* 
    Authored by Sashen, on behalf of "Estate Agent commission bid system"
*/

CREATE PROCEDURE FindBidDetails @Amenity_Description varchar(50)
AS
Select Property.Property_ID, Property.NumberOfRooms,Property.NumberOfBathrooms,Property.ERF, 
Property.StreetNumber,Property.StreetName, Property.City, Property.PostalCode ,Auction.Start_Date as BidSessionStart,
Auction.End_Date as BidSessionEnd, Auction.Location from Property  INNER JOIN Auction on Property.Property_ID 
= Auction.Property_ID where Property.Property_ID =(Select Property_ID from PropertyAmenity where Amenity_ID = 
(Select Amenity_ID from Amenity where Amenity_Description = @Amenity_Description))
GO;

/*

Problem statement:

1) As an estate agent, clients often look for certain specific ammenities in a property.
2) An example of this is a client looking for a pool.
3) As an estate agent, I want to bid on (and acquire the mandate for) properties that
match the needs of my buyer, e.g. I want to bid on properties that have a pool.
4) In order to inform my bid, I want to view the details of propeties (and the bid sessions 
thereof) that match the required attributes.

To execute this stored procedure, make use of the command:

"EXEC FindBidDetails @Amenity_Description = 'Pool';"

Where @Amenity_Description is the name of the attribute that you are searching by.

As a result, the query will return the PropertyID, NumberOfRooms, NumberOfBathrooms, ERF,
StreetNumber, StreetName, City, PostalCode, BidSessionStart, BidSessionEnd and Location of 
properties matching the provided ammenity description.

*/